<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	App\User::create([
    		'name'=>'Juan Sebastian',
    		'username'=>'juansebastianjsv',
    		'password'=>bcrypt('juansebastianjsv'),
    		'email'=>'juansebastian@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Jess Jonathan',
    		'username'=>'j.e.w.z',
    		'password'=>bcrypt('j.e.w.z'),
    		'email'=>'j.e.w.z@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Matthew Senangsa',
    		'username'=>'matthewsenangsa',
    		'password'=>bcrypt('matthewsenangsa'),
    		'email'=>'matthewsenangsa@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Owen Tandri',
    		'username'=>'owent',
    		'password'=>bcrypt('owent'),
    		'email'=>'owent@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Jason Alexander Chu',
    		'username'=>'jasonchu',
    		'password'=>bcrypt('jasonchu'),
    		'email'=>'jasonchu@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Hanvey Xavero',
    		'username'=>'hanvey',
    		'password'=>bcrypt('hanvey'),
    		'email'=>'hanvey@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Bryant Andretti Hasan Ho',
    		'username'=>'bryantho',
    		'password'=>bcrypt('bryantho'),
    		'email'=>'bryantho@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Graemie Dionysis',
    		'username'=>'graemie',
    		'password'=>bcrypt('graemie'),
    		'email'=>'graemie@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Jerel Hartono',
    		'username'=>'jerel',
    		'password'=>bcrypt('jerel'),
    		'email'=>'jerel@doggo.com',
    		'admin'=>'1',



    	]);

    	App\User::create([
    		'name'=>'Marcel Kurniawan',
    		'username'=>'marcel',
    		'password'=>bcrypt('marcel'),
    		'email'=>'marcelkurniawan@doggo.com',
    		'admin'=>'1',



    	]);
        // $this->call(UsersTableSeeder::class);
    }
}
